rm(list=ls())
cat("\014")
#dev.off()
setwd("~/Thesis")

library(readr)
library(ggplot2)
library(gridExtra)
library(tree)
library(rpart)
library(caret)
library(dplyr)
library(randomForest)

set.seed(1)

# Data --------------------------------------------------------------------

#full_table <- read.delim("/home/luna003/Thesis/repos/Thesis/Smallp_final_table_wSNP.tsv")
full_table <- read.delim("~/Thesis/Bigp_last.txt")
numeric_table <- full_table[,c(8:ncol(full_table))]

numeric_table$Label <- ifelse(numeric_table$GC >=35.84, "HIGH", "LOW")

newtable <- numeric_table[,c(2,19, 892)]


ggplot(numeric_table, aes(x= Label, y=LODscore)) + 
  geom_boxplot()
