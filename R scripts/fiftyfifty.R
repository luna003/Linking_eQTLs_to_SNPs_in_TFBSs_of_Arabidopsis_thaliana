rm(list=ls())
cat("\014")
dev.off()
setwd("~/Thesis")

library(readr)
library(ggplot2)
library(gridExtra)
library(tree)
library(rpart)
library(caret)
library(dplyr)
library(randomForest)

set.seed(1)

# Data --------------------------------------------------------------------

#full_table <- read.delim("/home/luna003/Thesis/repos/Thesis/Smallp_final_table_wSNP.tsv")
full_table <- read.delim("~/Thesis/Smallp_final_table_wSNP.tsv")
numeric_table <- full_table[,c(8:ncol(full_table))]

###############################################
#50-50 Data
###############################################
numeric_table$LODscore[numeric_table$LODscore<5] <- 0
numeric_table$LODscore[numeric_table$LODscore>=5] <- 1

numeric_table$LODscore<-as.factor(numeric_table$LODscore)
eqtls<- subset(numeric_table, LODscore == 1)
noneqtls<- subset(numeric_table, LODscore == 0)

somenoneqtls <- noneqtls[sample(nrow(noneqtls), nrow(eqtls)), ]

fiftyfifty <- rbind(eqtls, somenoneqtls)

##############################
#Classification tree
##############################

# Formula 1 (no motifs included) --------------------------------------------------------------------

formula <- LODscore ~ Distance + SNPs + Type_NC + Type_threeprime + Type_fiveprime + Type_intron + Type_spl + Type_nonsyn + Type_syn + Type_nonsens + Motifs + SNPs_in_motif + NT_of_motif + Relative_position + Max_Score + Avg_Score + nozeroAvg_Score +GC
simple_tree <- tree(formula, data = fiftyfifty)
summary(simple_tree)

# Because it only uses amount SNPs, we try taking that off
formula_noSNP <- LODscore ~ Distance + Type_NC + Type_threeprime + Type_fiveprime + Type_intron + Type_spl + Type_nonsyn + Type_syn + Type_nonsens + Motifs + SNPs_in_motif + NT_of_motif + Relative_position + Max_Score + Avg_Score + nozeroAvg_Score +GC
simple_tree_noSNP <- tree(formula_noSNP, data = fiftyfifty)
summary(simple_tree_noSNP)

#Now it only uses relative Position, so we also take it off
formula_noSNP_noRP <- LODscore ~ Distance + Type_NC + Type_threeprime + Type_fiveprime + Type_intron + Type_spl + Type_nonsyn + Type_syn + Type_nonsens + Motifs + SNPs_in_motif + NT_of_motif + Max_Score + Avg_Score + nozeroAvg_Score +GC
simple_tree_noSNP_noRP <- tree(formula_noSNP_noRP, data = fiftyfifty)
summary(simple_tree_noSNP_noRP)


# Formula 2 (all motifs included) --------------------------------------------------------------------

formula <- LODscore ~ .
simple_tree <- tree(formula, data = fiftyfifty)
summary(simple_tree)

# Because it only uses amount SNPs, we try taking that off
formula_noSNP <- LODscore ~ . -SNPs
simple_tree_noSNP <- tree(formula_noSNP, data = fiftyfifty)
summary(simple_tree_noSNP)

#Now it only uses relative Position, so we also take it off
formula_noSNP_noRP <- LODscore ~ . -SNPs -Relative_position
simple_tree_noSNP_noRP <- tree(formula_noSNP_noRP, data = fiftyfifty)
summary(simple_tree_noSNP_noRP)

# 
# tree.probs = predict(simple_tree, typpe = "response")
# tree.probs[1:10]
# 
# contrasts(numeric_table$LODscore)




###############################
#GLM
###############################
# Formula 1 (no motifs included) --------------------------------------------------------------------
formula <- LODscore ~ Distance + SNPs + Type_NC + Type_threeprime + Type_fiveprime + Type_intron + Type_spl + Type_nonsyn + Type_syn + Type_nonsens + Motifs + SNPs_in_motif + NT_of_motif + Relative_position + Max_Score + Avg_Score + nozeroAvg_Score +GC

# all data --------------------------------------------------------------------

glmodel <- glm(formula, numeric_table, family = 'binomial' )
summary(glmodel) # Aparentely GC is a better predictor than max score?

glm.probs = predict(glmodel, typpe = "response")
glm.probs[1:10]

contrasts(numeric_table$LODscore)
#shows that predicts for eQTLs ("1")
glm.pred = rep(0, nrow(numeric_table))
glm.pred[glm.probs>0.5] = 1
table(glm.pred, numeric_table$LODscore)
prediction.rate <- mean(glm.pred == numeric_table$LODscore)
training_error_glm <- 1- prediction.rate
training_error_glm
#numeric_table$PREDICTION <- glm.pred

# fiftyfifty data --------------------------------------------------------------------
glmodel <- glm(formula, fiftyfifty, family = 'binomial' )
summary(glmodel) # Aparentely GC is a better predictor than max score?

glm.probs = predict(glmodel, typpe = "response")
glm.probs[1:10]

contrasts(fiftyfifty$LODscore)
#shows that predicts for eQTLs ("1")
glm.pred = rep(0, nrow(fiftyfifty))
glm.pred[glm.probs>0.5] = 1
table(glm.pred, fiftyfifty$LODscore)
prediction.rate <- mean(glm.pred == fiftyfifty$LODscore)
training_error_glm <- 1- prediction.rate
training_error_glm

#fiftyfifty$PREDICTION <- glm.pred
# Formula 2 (all motifs included) --------------------------------------------------------------------
formula <- LODscore ~ .
glmodel <- glm(formula, fiftyfifty, family = 'binomial' )
summary(glmodel) # Aparentely GC is a better predictor than max score?

glm.probs = predict(glmodel, typpe = "response")
glm.probs[1:10]

contrasts(fiftyfifty$LODscore)
#shows that predicts for eQTLs ("1")
glm.pred = rep(0, nrow(fiftyfifty))
glm.pred[glm.probs>0.5] = 1
table(glm.pred, fiftyfifty$LODscore)
prediction.rate <- mean(glm.pred == fiftyfifty$LODscore)
training_error_glm <- 1- prediction.rate
training_error_glm
#numeric_table$PREDICTION <- glm.pred
##################################
######################################################
#Random Forest
######################################################
# Formula 1 (no motifs included) --------------------------------------------------------------------
formula <- LODscore ~ Distance + SNPs + Type_NC + Type_threeprime + Type_fiveprime + Type_intron + Type_spl + Type_nonsyn + Type_syn + Type_nonsens + Motifs + SNPs_in_motif + NT_of_motif + Relative_position + Max_Score + Avg_Score + nozeroAvg_Score +GC

#bagging
bag.fifty=randomForest(formula,data=fiftyfifty, mtry =18, importance=TRUE)
bag.fifty
#importance(bag.fifty)
varImpPlot(bag.fifty)

#random forest
forest.fifty=randomForest(formula,data=fiftyfifty, importance=TRUE)
forest.fifty

#importance(forest.fifty)
varImpPlot(forest.fifty)


# Formula 2 (all motifs included) --------------------------------------------------------------------
formula <- LODscore ~ .
#bagging
bag.fifty=randomForest(formula,data=fiftyfifty, mtry =890, importance=TRUE)
bag.fifty
#importance(bag.fifty)
varImpPlot(bag.fifty)

#random forest
forest.fifty=randomForest(formula,data=fiftyfifty, importance=TRUE)
forest.fifty

#importance(forest.fifty)
varImpPlot(forest.fifty)

