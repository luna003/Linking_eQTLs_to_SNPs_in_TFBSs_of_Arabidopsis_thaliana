ua#!usr/bin/env python 

from sys import argv
from pandas import read_table
import time


"""
Script that finds all genes in a GTF file with a 5UTR, 
then keeps TSS and direction of the gene.

Then finds all motif that are predicted diferently in two strains to place them
in the promoter region of the gene.
(+200 upstream and -1000 downstream are the default values)
(if the p-value of the prediction of the motif is diferent,
they are also considered as different)

Finally,  places every SNP in the promoter and keeps track of how many they are
in the whole promoter and also to the motifs inside the promoter.

4 tab separated table are made with all the information gathered for each gene \
and each motif in the promoter for both diff_motif and equal_motif.

To execute it from the command line you need this files:
    python This_script.py (1)genes.gtf (2)markerfile.txt (3)LODscoreFile.txt\
    (4)snps, (5)mast_output_varant1, (6)mast_output_variant2 (7)diff_genes.txt \
    (8)equal_genes.txt (9)diff_tfbs.txt (10)equal_tfbs.txt
    
Copy pasted command:
    python ~/Thesis/repos/Thesis/Table_generator.py genes_tair11.gtf marker.txt lod.txt unmatching_SNPs.txt mast_output_sha mast_output_bay diff_motif.txt equal_motif.txt diff_genes.txt equal_genes.txt
    
"""

#All "parse" function make a list for each entry of the file
def parse_snp(snpfile):
    snips = []
    for line in snpfile:
        line = line.strip()
        if not line.startswith('Chr'):
            record = line.split("\t")
            record[1] = int(record[1])
            snips += [record]
    return snips


def parse_markers(markerfile):
    markers = []
    for line in markerfile:
        line = line.strip()
        if not line.startswith('marker'):
            record = line.split("\t")
            markers += [record]
    print 'Number of markers: ', len(markers)
    return markers


def parse_GTF(GTFfile):
    genes = []
    for line in GTFfile:
        line = line.strip()
        record = line.split("\t")
        genes += [record]
    return genes


def parse_mastoutput(mastfile):
    tfbs = []
    for line in mastfile:
        line = line.strip()
        if not line.startswith('#'):
            record = line.split()
            tfbs += [record]
    return tfbs


def get_motifs (mastfile1, mastfile2):
    """
    Compares the prediction of MAST (hit_list) for the two strains and returns 
    a list 
    """
    first_file = []
    second_file = []
    for i in range(len(mastfile1)):
        chr1 = mastfile1[i][0]
        motif1 = mastfile1[i][1]
        start1 = mastfile1[i][2]
        end1 = mastfile1[i][3]
        pvalue1 = mastfile1[i][-1]
        record1 = (chr1, motif1, int(start1), int(end1), float(pvalue1))
        first_file.append(record1)    
    for i in range(len(mastfile2)):
        chr2 = mastfile2[i][0]
        motif2 = mastfile2[i][1]
        start2 = mastfile2[i][2]
        end2 = mastfile2[i][3]
        pvalue2 = mastfile2[i][-1]
        record2 = (chr2, motif2, int(start2), int(end2), float(pvalue2))
        second_file.append(record2)      
    in1_not2 = set(first_file) - set(second_file)
    in2_not1 = set(second_file) - set(first_file)
    diff_motifs = list(in1_not2) + list(in2_not1)
    repetitions = set(first_file) & set(second_file)
    total_motifs = list(repetitions) + diff_motifs
    print 'Number of motifs in first file: ', len(mastfile1)
    print 'Number of motifs in second file: ', len(mastfile2)
    print 'Number of identical motifs between them: ',len(list(repetitions))
    print 'Number of different motifs between them (if P-value is different is \
also considered as a different motif): ',len(diff_motifs)
    print 'Number of total motifs: ', len(total_motifs)
    return list(repetitions), diff_motifs


def get_tss(genes):
    tss_dict ={}
    for gene in genes:
        if gene[2] == '5UTR':
            chromosome = gene[0]
            #tss = int(gene[3])
            raw_gene = gene[-1]
            genename = raw_gene.split("\"")[3]
            direction = gene[6]
            
            if direction == '+':
                tss = int(gene[3])
                if genename in tss_dict:
                    new_tss = tss
                    old_tss = tss_dict[genename][1]
                    if old_tss > new_tss:
                        tss_dict[genename] = [chromosome[-1], new_tss, direction]
                    else:
                        pass
                else:
                    tss_dict[genename] = [chromosome[-1], tss, direction]
            
            if direction == '-':
                tss = int(gene[4])
                if genename in tss_dict:
                    new_tss = tss
                    old_tss = tss_dict[genename][1]
                    if old_tss < new_tss:
                        tss_dict[genename] = [chromosome[-1], new_tss, direction]
                    else:
                        pass
                else:
                    tss_dict[genename] = [chromosome[-1], tss, direction]

    return tss_dict
 
           
def get_marker(markers_parsed, tss_dict):
    marker_dict={}
    for gene in tss_dict:
        chromosome_gene = tss_dict[gene][0]
        tss = tss_dict[gene][1]
        direction = tss_dict[gene][2]
        for entry in markers_parsed:
            marker = entry[0]
            chromosome_marker = entry[1]
            position = int(entry[2])*10
            if chromosome_gene == chromosome_marker:
                distance = abs(tss-position)
                if gene in marker_dict:
                    old = marker_dict[gene][-1]
                    new = distance
                    if old>new:
                        marker_dict[gene] =[chromosome_marker, tss, direction, marker, new]
                    else:
                        pass
                else:
                    marker_dict[gene] =[chromosome_marker, tss, direction, marker, distance]
    return marker_dict

def get_allgenes(lod_table, marker_dict, lodthreshold=5, distancethreshold = 5000000000000000000000000000000000000000000000000):
    print 'LOD threshold:', lodthreshold
    eQTLs_dict = {}
    genes_wo_LOD = []
    for gene in marker_dict:
        if gene in lod_table.index:
            marker = marker_dict[gene][3]
            distance = marker_dict[gene][4]
            direction = marker_dict[gene][2]
            lodscore =  lod_table.get_value(gene, marker)
            
            eQTLs_dict[gene] = [marker_dict[gene][0], marker_dict[gene][1], \
            direction, marker, distance, lodscore]
            
        else:
            genes_wo_LOD += [gene]
    return eQTLs_dict, genes_wo_LOD


def place_SNPs_in_motifs(snips, motifs, eQTLs_dict, filename, promoter_negative = 500, \
promoter_positive = 0, threshold = float(1e-1)):
    print 'Promoter region: -%i +%i' %(promoter_negative, promoter_positive)
    print 'P-value threshold for the motif: ', threshold
    
    
    snips = sorted(snips,key = lambda x: (x[0], x[1]))
    motifs = sorted(motifs,key = lambda x: (x[0], x[2]))
    genes = sorted(eQTLs_dict.keys(),key = lambda x: (eQTLs_dict[x][0], eQTLs_dict[x][1]))
    
    motif_dict = {}
    #index_reset = 10000
    i_motifs = 0
    i_snips_g = 0
    i_snips_m = 0
    past_pvalue = 0
    max_index_snips = len(snips)-1
    max_index_motifs = len(motifs)-1
    
    fil = open(filename, 'w')
    fil.write('Gene\tChr\tTSS\tDirection\tMotif\tStart\tEnd\tP-value\tSNP position')
    fil.write('\n')
    other = 0
    for gene in genes:
        i_motifs = 0
        i_snips_g = 0
        i_snips_m = 0
        nc = 0
        threeprime = 0
        fiveprime = 0
        intron = 0
        nonsyn = 0
        syn = 0
        nonsense = 0
        spl = 0 
        

        amount_motifs = 0
        amount_snps_in_motif = 0
        amount_snps = 0
        nt_motif_in_promoter = 0
        
        chr_g = int(eQTLs_dict[gene][0])
        tss = eQTLs_dict[gene][1]
        direction = eQTLs_dict[gene][2]
        if direction == '+':
            start = tss - promoter_negative
            if start < 0:
                start = 0
            end = tss + promoter_positive
            
        elif direction == '-':
            start = tss - promoter_positive
            if start < 0:
                start = 0
            end = tss + promoter_negative
            
        
            
        else:
            print 'Direction in gene %s is not + or -. Direction found: %s' %(gene, direction)
        
        if max_index_snips > i_snips_g:
            while int(snips[i_snips_g][0]) < chr_g:
                
                if max_index_snips <= i_snips_g:
                    break
                else:
                    i_snips_g += 1
    
        if max_index_snips > i_snips_g:
        #scanning for the next SNP in the gene
            while int(snips[i_snips_g][0]) == chr_g and int(snips[i_snips_g][1]) < start :
                
                if max_index_snips <= i_snips_g:
                    break
                else:
                    i_snips_g += 1
   
        if max_index_snips >= i_snips_g:
        #counting the SNPs in the gene
            while int(snips[i_snips_g][0]) == chr_g and int(snips[i_snips_g][1]) <= end :
                amount_snps += 1
                if snips[i_snips_g][-1] == "NC":
                    nc += 1
                elif snips[i_snips_g][-1] == "3\'":
                    threeprime += 1
                elif snips[i_snips_g][-1] == "5\'":
                    fiveprime += 1
                elif snips[i_snips_g][-1] == "Int":
                    intron += 1
                elif snips[i_snips_g][-1] == "Non-Syn":
                    nonsyn += 1
                elif snips[i_snips_g][-1] == "Syn":
                    syn += 1
                elif snips[i_snips_g][-1] == "Nonsense":
                    nonsense += 1
                elif snips[i_snips_g][-1] == "Spl":
                    spl += 1
                else:
                    other += 1
                    print 'Strange label found in SNP: ', snips[i_snips_g]
                
                
                if max_index_snips <= i_snips_g:
                    break
                else:
                    i_snips_g += 1
                
        if max_index_motifs > i_motifs:
          
            while int(motifs[i_motifs][0]) < chr_g:
                
                if max_index_motifs <= i_motifs:
                    break
                else:
                    i_motifs += 1
        
        if max_index_motifs > i_motifs:
            #scanning for the next motif
            while int(motifs[i_motifs][0]) == chr_g and int(motifs[i_motifs][2]) < start :
                
                if max_index_motifs <= i_motifs:
                    break
                else:
                    i_motifs += 1
                
        if max_index_motifs > i_motifs:
            
            #analysing the motifs in the gene
            while int(motifs[i_motifs][0]) == chr_g and int(motifs[i_motifs][3]) <= end :
                
                positions = []
                motifname = motifs[i_motifs][1]
                start_motif = int(motifs[i_motifs][2])
                end_motif = int(motifs[i_motifs][3])
                pvalue = motifs[i_motifs][-1]
                nts = end_motif - start_motif
                if pvalue <= threshold:
                    past_pvalue += 1
                    amount_motifs += 1
                    nt_motif_in_promoter += nts
                    if max_index_snips > i_snips_m:
                        while int(snips[i_snips_m][0]) < chr_g  :                    
                            
                            if max_index_snips <= i_snips_m:
                                break
                            else:
                                i_snips_m += 1                                
                    
                    if max_index_snips > i_snips_m:
        
                        #scanning for the next SNP in the motif
                        while int(snips[i_snips_m][0]) == chr_g and int(snips[i_snips_m][1]) < start_motif :                        
    
                            if max_index_snips <= i_snips_m:
                                break
                            else:
                                i_snips_m += 1
                                
                    if max_index_snips >= i_snips_m:
                        
                        #counting the SNPs in the motif
                        
                        while int(snips[i_snips_m][0]) == chr_g and int(snips[i_snips_m][1]) <= end_motif :                    
                            positions.append(int(snips[i_snips_m][1]))
                            amount_snps_in_motif += 1
                            
                            if max_index_snips <= i_snips_m:
                                break
                            else:
                                i_snips_m += 1
                                
                if max_index_snips <= i_snips_m:
                    break
                   
                            
                if len(positions) != 0:
                    fil.write('%s\t%i\t%i\t%s\t%s\t%i\t%i\t%e\t'\
    %(gene, chr_g, tss, direction, motifname, start_motif, end_motif, pvalue))
    
                    for i in range(len(positions)):
                        
                        fil.write('%i' %(positions[i]))
                        if len(positions) > 1 and i != len(positions)-1:
                            fil.write(',')
                    fil.write('\n')
                else:
                    fil.write('%s\t%i\t%i\t%s\t%s\t%i\t%i\t%e\t-\n'\
    %(gene, chr_g, tss, direction, motifname, start_motif, end_motif, pvalue))
                                
                i_motifs += 1
            
        motif_dict[gene] = [chr_g, tss, direction, start, end, eQTLs_dict[gene][3], eQTLs_dict[gene][4],\
                   eQTLs_dict[gene][5], amount_snps, nc, threeprime, fiveprime, intron, spl, nonsyn, syn, nonsense, amount_motifs, amount_snps_in_motif, nt_motif_in_promoter]
    print 'Different than NC SNPs: ', other
    print 'Motifs:', i_motifs, 'SNPs:', i_snips_g, i_snips_m
    return motif_dict


def create_table_from_dict (dictionary, filename):
    fil = open(filename, 'w')
    fil.write('Gene\tChr\tTSS\tDirection\tStart_Promoter\tEnd_Promoter\tMarker\tDistance\tLODscore\tSNPs\t')
    fil.write('Type_NC\tType_threeprime\tType_fiveprime\tType_intron\tType_spl\tType_nonsyn\tType_syn\tType_nonsens\tMotifs\tSNPs_in_motif\tNT_of_motif')
    fil.write('\n')
    for entry in sorted(dictionary):
        fil.write(str(entry))
        fil.write('\t')
        for i in range(len(dictionary[entry])):
            fil.write(str(dictionary[entry][i]))
            if i != len(dictionary[entry]) - 1:
                fil.write('\t')
            else:
                fil.write('\n')
        
# =============================================================================
#         fil.write('%s\t%i\t%i\t%s\t%i\t%i\t%s\t%s\t%f\t%i\t%i\t%i\t%i\n'\
# %(entry, dictionary[entry][0], dictionary[entry][1], dictionary[entry][2], dictionary[entry][3],\
# dictionary[entry][4], dictionary[entry][5], dictionary[entry][6], dictionary[entry][7],\
#  dictionary[entry][8], dictionary[entry][9], dictionary[entry][10], dictionary[entry][11]))
# 
# =============================================================================
        
if __name__ == '__main__':
    startTime = time.clock()
    directory = '/home/luna003/Thesis/Data/'
    gtf_file = open(directory + argv[1])
    genes = parse_GTF(gtf_file)
    tss_dict = get_tss(genes)
    
    print 'Number of genes got from the .gtf file: %i'%(len(tss_dict))
    
#This genes is just the earliest 5UTR of each gene
    marker_file = open(directory + argv[2]) 
    markers = parse_markers(marker_file)
    genes_dict_withmarker = get_marker(markers, tss_dict)
    print 'Number of chromosomal genes: %i' %(len(genes_dict_withmarker))
    print '--------------'
    lodfile = open(directory + argv[3])
    lod_table = read_table(lodfile, index_col=0)
    all_genes, genes_nf = get_allgenes(lod_table, genes_dict_withmarker)
    snpsfile = open(directory + argv[4])
    snips = parse_snp(snpsfile)
    print 'Number of SNPs: ', len(snips)
    motiffile1 = open(directory + argv[5])
    motiffile2 = open(directory + argv[6])
    motifs1 = parse_mastoutput(motiffile1)
    motifs2 = parse_mastoutput(motiffile2)
    rep_motifs, diff_motifs = get_motifs(motifs1, motifs2) 
    
    #For diff motif
    diff_motif_output = argv[7]
    diff_motif_all_genes = place_SNPs_in_motifs(snips, diff_motifs, all_genes, diff_motif_output )
    table_file_name = argv[9]
    create_table_from_dict(diff_motif_all_genes, table_file_name)
    spent_time1 = time.clock() - startTime
    print 'Time: %f min' %(spent_time1/60)
    print 'Tables for diff motif created'

    #For equal motif
    rep_motif_output = argv[8]
    rep_motif_all_genes = place_SNPs_in_motifs(snips, rep_motifs, all_genes, rep_motif_output )
    table_file_name = argv[10]
    create_table_from_dict(rep_motif_all_genes, table_file_name)
    print 'Tables for equal motif created'    
    spent_time2 = time.clock() - startTime
    print 'Time: %f min' %(spent_time2/60)

