#!usr/bin/env python 

from sys import argv

import re


#from pandas import read_table
#import time


"""
Identifies the SNP in the motif that is making the prediction of the motif different
and gives a score. Each gene that has a SNP in one of the motif of the promoter gets a score

to run it:
python update_motiftable.py ~/Thesis/Data/dap_data_v4/merged_motifs.txt SNP_bay_vs_sha.txt Motifs.txt Score_table.txt
"""

def parse_mergedmotifs (motiffile):
    
    motifs_dict ={}
    records = motiffile.split('MEME version ')
    motif_index = 0 
    for record in records:
        version = record[:4]
        probability_matrix = []
        record = record.partition('ALPHABET= ACGT')[2]
        record = record.strip()
        if record.startswith('strands') and version == '4.10':
            motif_index +=1
            motifnamesearch = re.search('ARID_tnt\.(.*) MEME', record)
            if motifnamesearch:
                    motifname = motifnamesearch.group(1)
                    
            statsearch = re.search('letter-probability matrix: alength= (.*) w= (.*) nsites= (.*) E= (.*)\n' , record)
            if statsearch:
                if statsearch.group(1):
                    alength = statsearch.group(1)
                if statsearch.group(2):
                    w = int(statsearch.group(2))
                lines = record.split('\n')
                values = lines[-w:]
                for value in values:
                    value = value. strip()
                    value = value.split(' ')
                    probability_matrix += [value]
                motifs_dict[int(motif_index)] = [motifname, int(alength), int(w), probability_matrix]        
                
        if record.startswith('strands') and version != '4.10':
            motif_index +=1
            motifnamesearch = re.search('ARID_tnt\.(.*) MEME', record)
            if motifnamesearch:
                    motifname = motifnamesearch.group(1)
                    
            statsearch = re.search('letter-probability matrix: alength= (.*) w= (.*) nsites= (.*) E= (.*)\n' , record)
            if statsearch:
                if statsearch.group(1):
                    alength = statsearch.group(1)
                if statsearch.group(2):
                    w = int(statsearch.group(2))
                lines = record.split('\n')
                values = lines[-w:]
                for value in values:
                    value = value. strip()
                    value = value.replace(' ', '')
                    value = value.split('\t')
                    probability_matrix += [value]
                motifs_dict[int(motif_index)] = [motifname, int(alength), int(w), probability_matrix]        
                    
    return motifs_dict
        
def parse_allsnips (snpfile):
    #raresnips = ['R', 'Y', 'S', 'W', 'K', 'M', 'N']
    normalsnips = ['A', 'C', 'G', 'T']
    snips = []
    leftovers = []
    for line in snpfile:
        line = line.strip()
        if not line.startswith('Chr'):
            record = line.split("\t")
            if record[-1] in normalsnips and record[-2] in normalsnips:
                
                snips += [record]
            else:
                leftovers +=  [record]
    print 'There are %i SNPs different than A, C, G or T\n' %len(leftovers)
    print 'The final count for SNPs usefull for calculating the score: ', len(snips)
    return snips

def parse_motiftable (motiftable):
    motifs = []
    for line in motiftable:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            motifs += [record]
    return motifs

def savediv (x,y):
    if y == 0 or x == 0:
        z = 0
    else:
        z = x/y
    return z 
        

def main (motifs_dict, snips, motifs):
    max_score_dict = {}
    aver_score_dict = {}   
    nozero_score_dict = {}
    
    for i in range(len(motifs)):
        all_scores_list = []
        zero_scores = 0
        
        
        gene = motifs[i][0]
        chrm = motifs[i][1]
        motif = int(motifs[i][4][1:])
        start = motifs[i][5]
        snp = motifs[i][-1]
        
        snp = snp.split(',')
        for z in range(len(snp)):
            
            for j in range(len(snips)):
                if snips[j][0] == chrm  and snips[j][1] == snp[z]:
                    bay = snips[j][-2]
                    sha = snips[j][-1]
                    relative_position = int(snp[z])-int(start)
                    A, C, G, T = motifs_dict[motif][3][relative_position]
                    if bay == 'A':
                        bay_score = A
                    if bay == 'C':
                        bay_score = C
                    if bay == 'G':
                        bay_score = G    
                    if bay == 'T':
                        bay_score = T
                        
                    if sha == 'A':
                        sha_score = A
                    if sha == 'C':
                        sha_score = C
                    if sha == 'G':
                        sha_score = G
                    if sha == 'T':
                        sha_score = T
                    score = (float(bay_score) - float(sha_score))**2
                    
                    if gene in max_score_dict:
                        old_score = max_score_dict[gene] 
                        if old_score < score:        
                            max_score_dict[gene] = score
                        else:
                            pass
                    else:
                        max_score_dict[gene] = score
                        
                     
                    all_scores_list.append(score)
        
        for score in all_scores_list:
            if score == 0:
                zero_scores += 1
            
        if len(all_scores_list) != 0 :
            sum_scores = float(sum(all_scores_list))
            total = float(len(all_scores_list))
            total_average = savediv(sum_scores, total)
            non_zero_total = float(total-zero_scores)
            nonzero_average = savediv(sum_scores, non_zero_total)
            nozero_score_dict[gene] = nonzero_average
            aver_score_dict[gene] = total_average
            
            if nonzero_average != total_average:
                print gene
    
    return max_score_dict, aver_score_dict, nozero_score_dict


def create_table_from_dict (dict1, dict2, dict3, filename):
    fil = open(filename, 'w')
    fil.write('Gene\tMax_Score\tAverage\tNon_0_average')
    fil.write('\n')
    for entry in sorted(dict1):
        fil.write('%s\t%f\t%f\t%f\n' %(entry, dict1[entry], dict2[entry], dict3[entry]))
    fil.close()
    
if __name__ == '__main__':
    #directory = '/home/luna003/Thesis/Data/Tables/'
    motiffile= open(argv[1])
    text = motiffile.read()
    motifs_dict = parse_mergedmotifs(text)
   
    
    snpfile = open(argv[2])
    snips = parse_allsnips(snpfile)
    
    motiftable = open(argv[3])
    motifs = parse_motiftable(motiftable)
    
    max_score, aver_score, nozero_score = main(motifs_dict,snips, motifs)
    output = argv[4]
    create_table_from_dict(max_score, aver_score, nozero_score, output)
    
    