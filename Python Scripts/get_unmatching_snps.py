#!usr/bin/env python 

from sys import argv
"""
to generate snips 

python ~/Thesis/repos/Thesis/get_unmatching_snps.py ~/Thesis/Data/Shadara.snp.aa.txt ~/Thesis/Data/Bay-0.snp.aa.txt real_SNPs.txt

"""

def parse_snp(snpfile):
    snips = []
    for line in snpfile:
        line = line.strip()
        if not line.startswith('#'):
            record = line.split("\t")
            snips += [record]
    return snips

def find_snips (snips1, snips2):
    
    first_file_full = []
    second_file_full = []
    real_snips = []
    for i in range(len(snips1)):
        chr_snp1 = snips1[i][0]
        position1 = int(snips1[i][1])
        
        new1 = snips1[i][12]
        old1 = snips1[i][5]
        record_full = [chr_snp1, position1, old1, new1]
        
        first_file_full.append(record_full)
        
    for i in range(len(snips2)):
        chr_snp2 = snips2[i][0]
        position2 = int(snips2[i][1])
        new2 = snips2[i][12]
        old2 = snips2[i][5]
        record2_full = [chr_snp2, position2, old2, new2]
        
        second_file_full.append(record2_full)

    print 'SNPs first file full: ', len(first_file_full)
    print 'SNPs second file full: ', len(second_file_full)
    first_set = set(map(tuple, first_file_full))
    second_set = set(map(tuple, second_file_full))
    
    real_snips = first_set.symmetric_difference(second_set)
    repetitions = first_set.intersection(second_set)
    real_snips_list = list(real_snips)

    print 'The amount of SNPs that are equal in both files is:',len(repetitions)
    print 'The amount of real SNPs is:',len(real_snips_list)
    
    snips = sorted(real_snips_list,key = lambda x: (x[0], x[1]))
    

    return snips

def make_file (real_snips, output_name):
    fil = open(output_name, 'w')
    fil.write('Chr\tPosition\tCol\tNew\n')
    for i in range(len(real_snips)):
        fil.write('%s\t%s\t%s\t%s\n'%(real_snips[i][0], real_snips[i][1], real_snips[i][2], real_snips[i][3]))
    fil.close()   

if __name__ == '__main__':
    snp_file1 = open(argv[1])
    snp_file2 = open(argv[2])
    snips1 = parse_snp(snp_file1)
    snips2 = parse_snp(snp_file2)
    data = find_snips (snips1, snips2)
    unmatching_snips = argv[3]
    make_file(data, unmatching_snips)
                    