#!usr/bin/env python 

from sys import argv
import time


"""
Make a single table merging the score table, relative_distance, motif and genes
"""

#All "parse" function make a list for each entry of the file
def parse_gene(table):
    genes_dict = {}
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            genes_dict[record[0]] = record[1:]
    return genes_dict

def parse_distance_or_gc(table):
    distance_dict = {}
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            distance_dict[record[0]] = record[-1]
    return distance_dict

def parse_score (three_scores_table):
    score_dict = {}
    for line in three_scores_table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            score_dict[record[0]] = record[1:]
    
    return score_dict

def add_value (genes_dict,distance_dict):
    new_dict = {}
    for gene in genes_dict:
        if gene in distance_dict:
            distance = distance_dict[gene]
            record = genes_dict[gene]
            
            record.append(distance)
            new_dict[gene] = record
    return new_dict

def add_list (genes_dict, distance_dict):
    new_dict = {}
    for gene in genes_dict:
        if gene in distance_dict:
            distance = distance_dict[gene]
            record = genes_dict[gene]
            
            record.extend(distance)
            new_dict[gene] = record
    return new_dict

def generate_output(dictionary, output_file):
    fil = open(output_file, 'w')
    fil.write('Gene\tChr\tTSS\tDirection\tStart_Promoter\tEnd_Promoter\tMarker\tDistance\tLODscore\tSNPs\t')
    fil.write('Type_NC\tType_threeprime\tType_fiveprime\tType_intron\tType_spl\tType_nonsyn\tType_syn\tType_nonsens\tMotifs\tSNPs_in_motif\tNT_of_motif\tRelative_position\tMax_Score\tAvg_Score\tnozeroAvg_Score\tGC')
    fil.write('\n')
    for entry in sorted(dictionary):
        fil.write(entry)
        fil.write('\t')
        for i in range(len(dictionary[entry])):
            fil.write(dictionary[entry][i])
            if i != len(dictionary[entry]) - 1:
                fil.write('\t')
            else:
                fil.write('\n')


if __name__ == '__main__':
    startTime = time.clock()
    
    genes = open(argv[1])
    distances = open(argv[2])
    scores = open(argv[3])
    gc = open(argv[4])
    
    with_distance = add_value(parse_gene(genes), parse_distance_or_gc(distances))
    with_score = add_list(with_distance, parse_score(scores))
    with_gc = add_value(with_score, parse_distance_or_gc(gc))

    output = argv[5]
    
    generate_output(with_gc, output)
    
    spent_time = time.clock() - startTime
    print 'Time: %f min' %(spent_time/60)