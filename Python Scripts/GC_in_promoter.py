#!usr/bin/env python 

from sys import argv
import time
#import pandas  as pd


"""
Make a file with all GC% of every promoter
"""
def parse_fasta(fastafile):
    seqs = {}
    for line in fastafile:
        line = line.strip()
        if line.startswith('>'):
            label = line.strip()[1:]
            number = label.split(" ")[0] 
            seqs[number] = []
        else:
            seqs[number].append(line.upper())
    for chr in seqs:
        seqs[chr] = ''.join(seqs[chr])
    return seqs
"""
def parse_table(table):
    table = pd.read_table(genes_table, header = 0, index_col = 0)
    genes_dict = table.to_dict('index')
    return genes_dict
"""

def parse_table(table):
    genes_dict = {}
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            genes_dict[record[0]] = record[1:]
    return genes_dict


def GC(seq): 
    """Calculates G+C content, returns the percentage (float between 0 and 100)"""
    gc = sum(seq.count(x) for x in ['G', 'C', 'g', 'c', 'S', 's']) 
    try: 
        return gc * 100.0 / len(seq) 
    except ZeroDivisionError: 
        return 0.0

def dict_to_table_file(dictionary, output_file):
    fil = open(output_file, 'w')
    fil.write('Gene\tChr\tGC%')
    fil.write('\n')
    for entry in sorted(dictionary):
        
        fil.write('%s\t%s\t%s\n'\
%(entry, dictionary[entry][0], dictionary[entry][1])) 
    fil.close()
    
        
        
   
def main(genes_table, seq_fasta, output_file):
    final_dict ={}
    genes = parse_table(genes_table)
    print 'Table of genes parsed'
    seqs = parse_fasta(seq_fasta)
    print 'Fasta file parsed'
    for gene in genes:
        chrom = genes[gene][0]
        start = int(genes[gene][3]) -1
        if start < 0:
            start = 0
        end = int(genes[gene][4]) -1
        promoter_seq = seqs[chrom][start:end]
        gc = GC(promoter_seq)
        final_dict[gene] = [chrom, gc]
    
    dict_to_table_file(final_dict, output_file)    
    print 'GC file created' 


if __name__ == '__main__':
    startTime = time.clock()
    directory = '/home/luna003/Thesis/Data/'
    genes_table = open(argv[1])
    seq_fasta = open(directory + argv[2])
    output = argv[3]
    main(genes_table, seq_fasta, output)
    spent_time = time.clock() - startTime
    print 'Time: %f min' %(spent_time/60)
    