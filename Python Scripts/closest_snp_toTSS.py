#!usr/bin/env python 

from sys import argv
import time


"""
Make a single table merging the score table, relative_distance, motif and genes
"""

#All "parse" function make a list for each entry of the file

def parse_position(table):
    record_list = []
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            row = line.split("\t")
            record_list.append(row)
    return record_list
            
            
            
def get_closest_to_zero (record_list):
    position_dict = {}
    for record in record_list:
        genename = record[0]
        relativepos = int(record[-1])
        
        if genename in position_dict:
            old_position = position_dict[genename]
            if abs(old_position) > abs(relativepos):
                position_dict[genename] = relativepos
            else:
                pass
        else:
            position_dict[genename] = relativepos
                
            
    return position_dict


def generate_output(dictionary, output_file):
    fil = open(output_file, 'w')
    fil.write('Gene\tRelative_Position')
    fil.write('\n')
    for entry in sorted(dictionary):
        
        fil.write('%s\t%s\n' %(entry, dictionary[entry]))
    fil.close()

if __name__ == '__main__':
    startTime = time.clock()
    positionsTable = open(argv[1])
    record_list = parse_position(positionsTable)
    position_dict = get_closest_to_zero(record_list)
    
    output = argv[2]
    
    generate_output(position_dict, output)
    
    spent_time = time.clock() - startTime
    print 'Time: %f min' %(spent_time/60)