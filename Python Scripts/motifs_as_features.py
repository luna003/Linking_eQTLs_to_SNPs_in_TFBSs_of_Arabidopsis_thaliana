#!usr/bin/env python 

from sys import argv
import time


"""
Make a single table merging the score table, relative_distance, motif and genes
"""

#All "parse" function make a list for each entry of the file
def parse_genes(table):
    genes_dict = {}
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            genes_dict[record[0]] = record[1:]
    return genes_dict

def parse_motifs (table):
    motif_list = []
    for line in table:
        line = line.strip()
        if not line.startswith('Gene'):
            record = line.split("\t")
            motif_list.append(record)
    return motif_list
    




def count_motifs (genes_dict, motifs_list):
    genes_count = {}
    for key in genes_dict:
        count_dict = {}
        for i in range(1,873):
            count_dict[str(i)] = 0
        for record in motifs_list:
            gene = record[0]
            motif_index_andsign = record[4]
            motif_index = motif_index_andsign[1:]
            
            if gene == key:
                count_dict[motif_index] += 1 
        genes_count[key] = count_dict
        
# =============================================================================
#         print key
#         print genes_count[key]['129']
#         print genes_count[key]['853']
#         print genes_count[key]['168']
#         print genes_count[key]['840']
#         print genes_count[key]['6']
#         print genes_count[key]['309']
#         print genes_count[key]['7']
#         print genes_count[key]['112']
#         print genes_count[key]['3']
# 
# =============================================================================
      
    return genes_count
        
                
                
        
        
        

def generate_output(genes_dict, genes_count, output_file):
    fil = open(output_file, 'w')
    fil.write('Gene\tChr\tTSS\tDirection\tStart_Promoter\tEnd_Promoter\tMarker\tDistance\tLODscore\tSNPs\tType_NC\tType_threeprime\tType_fiveprime\tType_intron\tType_spl\tType_nonsyn\tType_syn\tType_nonsens\tMotifs\tSNPs_in_motif\tNT_of_motif\tRelative_position\tMax_Score\tAvg_Score\tnozeroAvg_Score\tGC')
    fil.write('\t')
    for i in range(873):
        if i != 0:
            fil.write('%i' % i)
            if i != 872:
                fil.write('\t')
            else:
                fil.write('\n')
    for entry in sorted(genes_dict):
        
        fil.write(entry)
        fil.write('\t')
        for i in range(len(genes_dict[entry])):
            
            fil.write(genes_dict[entry][i])
            fil.write('\t')
        for i in range(len(genes_count[entry])+1):
# =============================================================================
#             strindex = str(i)
#             print strindex
# =============================================================================
            if i != 0:
                
                writethis = genes_count[entry]['%i' %i]
                writethis = str(writethis)
                #print genes_count[entry]['%i' %i]
                fil.write(writethis)
                if i != len(genes_count[entry]) : #-1 si quiero la tabla q ya estaba hecha
                    
                    fil.write('\t')
                else:
                    fil.write('\n')

if __name__ == '__main__':
    startTime = time.clock()
    
    genetable = open(argv[1])
    motiftable = open(argv[2])
    output = argv[3]
    
    genes_dict = parse_genes(genetable)
    motifslist = parse_motifs(motiftable)
    
    genes_count = count_motifs(genes_dict, motifslist)
    
    
    generate_output(genes_dict, genes_count, output)
    
    spent_time = time.clock() - startTime
    print 'Time: %f min' %(spent_time/60)
